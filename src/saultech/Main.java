package saultech;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) {
	// write your code here
       // System.out.println(Arrays.toString(compare(new int[]{3, 5, 2, 4, 6, 1}, new int[]{5, 2, 6, 7, 5, 2})));
        System.out.println(performCalculation("11","11","-"));
    }
    private static int[] compare(int[] a, int[] b){
        int[] result  = IntStream.range(0, a.length)
                .map(i -> Integer.compare(a[i], b[i]))
                .toArray();
        return new int[]{(int) Arrays.stream(result).filter(i -> i > 0).count(),
                (int) Arrays.stream(result).filter(i -> i < 0).count()};
    }


//Assumption +ve interger ... no sigh, no .,no float resut is within range of int
    private static String performCalculation(String firstNum, String secondNum,String operator){
        return switch (operator) {
            case "+" -> sum(firstNum, secondNum);
            case "-" -> subtract(firstNum, secondNum);
            case "x" -> multiply(firstNum, secondNum);
            default -> divide(firstNum, secondNum);
        };

    }
    private static String sum(String firstNum, String secondNum){
        StringBuilder sb = new StringBuilder();
        int carry = 0;
        int i = firstNum.length() - 1;
        int j = secondNum.length() - 1;

        while (i > -1 || j > -1) {
            int sum = carry + (i < 0 ? 0 : firstNum.charAt(i--) - 48);
            sum += j < 0 ? 0 : secondNum.charAt(j--) - 48;
            sb.append(sum % 10);
            carry = sum / 10;
        }
        return sb.append(carry == 1 ? "1" : "").reverse().toString();

    }
    private static String subtract(String firstNum, String secondNum){

        if (isSmaller(firstNum, secondNum)) {
            String t = firstNum;
            firstNum = secondNum;
            secondNum = t;
        }

        StringBuilder str = new StringBuilder();

        int n1 = firstNum.length(), n2 = secondNum.length();
        int diff = n1 - n2;

        int carry = 0;

        for (int i = n2 - 1; i >= 0; i--) {

            int sub
                    = (((int)firstNum.charAt(i + diff) - (int)'0')
                    - ((int)secondNum.charAt(i) - (int)'0')
                    - carry);
            if (sub < 0) {
                sub = sub + 10;
                carry = 1;
            }
            else
                carry = 0;

            str.append(String.valueOf(sub));
        }

        // subtract remaining digits of str1[]
        for (int i = n1 - n2 - 1; i >= 0; i--) {
            if (firstNum.charAt(i) == '0' && carry > 0) {
                str.append("9");
                continue;
            }
            int sub = (((int)firstNum.charAt(i) - (int)'0')
                    - carry);
            if (i > 0 || sub > 0) // remove preceding 0's
                str.append(sub);
            carry = 0;
        }

        // reverse resultant string
        return new StringBuilder(str.toString()).reverse().toString();

    }
    private static String multiply(String firstNum, String secondNum){

        char[] n1 = firstNum.toCharArray();
        char[] n2 = secondNum.toCharArray();

        int result = 0;

        for (int i = 0; i < n1.length; i++) {
            for (int j = 0; j < n2.length; j++) {
                result += (n1[i] - '0') * (n2[j] - '0')
                        * (int) Math.pow(10, n1.length + n2.length - (i + j + 2));
            }
        }
        return String.valueOf(result);
    }
    private static String divide(String firstNum, String secondNum){
        StringBuilder result
                = new StringBuilder();

        char[] dividend
                = firstNum.toCharArray();

        int carry = 0;
        int divisor = Integer.parseInt(secondNum);

        for (
                int i = 0;
                i < dividend.length; i++) {
            int x
                    = carry * 10
                    + Character.getNumericValue(
                    dividend[i]);

            result.append(x / divisor);

            // Prepare the carry for
            // the next Iteration
            carry = x % divisor;
        }

        // Remove any leading zeros
        for (
                int i = 0;
                i < result.length(); i++) {
            if (
                    result.charAt(i) != '0') {
                // Return the result
                return result.substring(i);
            }
        }

        return "";
    }
    static boolean isSmaller(String str1, String str2)
    {
        // Calculate lengths of both string
        int n1 = str1.length(), n2 = str2.length();

        if (n1 < n2)
            return true;
        if (n2 < n1)
            return false;

        for (int i = 0; i < n1; i++) {
            if (str1.charAt(i) < str2.charAt(i))
                return true;
            else if (str1.charAt(i) > str2.charAt(i))
                return false;
        }
        return false;
    }




    }
